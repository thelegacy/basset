<?php

if ( ! function_exists('basset_javascripts'))
{
    function basset_javascripts()
    {
        return app('basset')->collections(func_get_args(), "js");
    }
}

if ( ! function_exists('basset_stylesheets'))
{
    function basset_stylesheets()
    {
        return app('basset')->collections(func_get_args(), "css");
    }
}


/* Needed for Lumen Application */
if ( ! function_exists('public_path'))
{
    /**
     * Get the $path to the public folder.
     *
     * @param  string  $path
     * @return string
     */
    function public_path($path = '')
    {
        return env('PUBLIC_PATH', base_path('public')).($path ? '/'.$path : $path);
    }
}
