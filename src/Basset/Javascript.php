<?php

namespace Basset;

use ArrayAccess;

class Javascript implements ArrayAccess
{
    /**
     * Data.
     *
     * @var array
     */
    private $data = [];

    public function &__get($key)
    {
        return $this->data[$key];
    }

    public function __set($key, $value)
    {
        return array_set($this->data, $key, $value);
    }

    public function __isset($key)
    {
        return isset($this->data[$key]);
    }

    public function __unset($key)
    {
        unset($this->data[$key]);
    }

    public function __toString()
    {
        return $this->output('preload');
    }

    public function __invoke($exports)
    {
        return $this->output($exports);
    }

    public function set($key, $value)
    {
        return array_set($this->data, $key, $value);
    }

    public function output($exports = 'preload')
    {
        return (
            '<script>'.
            '!function(w,e,d){'.
            'w[e]=d'.
            '}(this,\''.$exports.'\','.json_encode($this->data).')'.
            '</script>'
        );
    }

    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->data[] = $value;
        } else {
            $this->data[$offset] = $value;
        }
    }

    public function offsetExists($offset)
    {
        return isset($this->data[$offset]);
    }

    public function offsetUnset($offset)
    {
        if ($this->offsetExists($offset)) {
            unset($this->data[$offset]);
        }
    }

    public function offsetGet($offset)
    {
        return $this->offsetExists($offset) ? $this->data[$offset] : null;
    }
}
