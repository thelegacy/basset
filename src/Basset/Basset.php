<?php

namespace Basset;

use Exception;
use Symfony\Component\Process\Process;

class Basset
{
    /**
     * Environment name.
     *
     * @var string
     */
    protected $env;

    /**
     * Basset manifest built with gulp.
     *
     * @var array
     */
    protected $manifest;

    /**
     * Public subdir of built files.
     *
     * @var string
     */
    protected $prefix = 'builds/';

    /**
     * @param string $manifestPath
     */
    public function __construct($manifestPath)
    {
        $contents = @file_get_contents($manifestPath);

        if ( ! $contents) {
            $process = new Process('gulp build', base_path());

            try {
                $process->run();

                if ($process->getExitCode() != 0) {
                    throw new Exception('Runtime error', 1);
                }
            } catch (Exception $e) {
                throw new Exception('Please run `gulp build` manually', 1);
            }

            $contents = file_get_contents($manifestPath);
        }

        $this->manifest = json_decode($contents, true);

        $this->env = array_get($this->manifest, 'environment', function() {
            $environment = app()->environment();

            return in_array($environment, ['local', 'development', 'dev'])
                   ? 'local'
                   : 'production';
        });
    }

    /**
     * @param array $collectionNames
     * @param $type
     *
     * @return string
     */
    public function collections(array $collectionNames, $type)
    {
        $return = '';
        foreach ($collectionNames as $collection) {
            $return .= $this->collection($collection, $type);
        }

        return $return;
    }

    /**
     * @param $collectionName
     * @param $type
     *
     * @return string
     */
    public function collection($collectionName, $type)
    {
        $env = $this->env;

        $files = array_get($this->manifest, "$collectionName.$env.$type", []);
        $returns = '';

        foreach ($files as $original => $built) {
            $built = $this->prefix.$built;

            // Check is raw file
            if (($pos = strpos($built, '//')) !== false && $pos >= 0) {
                $publicFile = $original;
            } else {
                $publicFile = url($built);
            }

            if ($type == 'js') {
                $returns .= '<script defer src="'.$publicFile.'"></script>'."\n";
            } elseif ($type == 'css') {
                $returns .= '<link rel="stylesheet" href="'.$publicFile.'"/>'."\n";
            }
        }

        return $returns;
    }
}
