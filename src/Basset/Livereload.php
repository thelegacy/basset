<?php

namespace Basset;

use Closure;
use Illuminate\Contracts\Foundation\Application;

class Livereload
{
    /**
     * The application implementation.
     *
     * @var Application
     */
    protected $app;

    /**
     * Create a new filter instance.
     *
     * @param Application $app
     */
    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure                 $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = $next($request);

        if ($this->app->environment('local', 'development', 'dev')) {
            $content = $response->getContent();
            if (strpos($content, $body = '</body') !== false) {
                $PORT = env('LIVERELOAD_PORT', 35729);
                $SERVER = $this->app['request']->server('SERVER_NAME').':'.$PORT;

                $snippet = '<script src="//'.$SERVER.'/livereload.js"></script>';
                $content = str_replace($body, $snippet.$body, $content);
                $response->setContent($content);
            }
        }

        return $response;
    }
}
