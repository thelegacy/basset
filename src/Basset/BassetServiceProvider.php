<?php

namespace Basset;

use Illuminate\Support\ServiceProvider;

class BassetServiceProvider extends ServiceProvider
{
    /**
     * Register the service provider.
     */
    public function register()
    {
        require_once __DIR__.'/../helpers.php';

        $this->app->singleton('basset', function ($app) {
            return new Basset(
                public_path('builds/manifest.json'),
                $app->environment()
            );
        });

        $this->app->singleton('Basset\Javascript', function ($app) {
            $js = new Javascript();
            $this->app->view->share('js', $js);

            return $js;
        });

        $this->registerBladeExtensions();
    }

    /**
     * Register the Blade extensions with the compiler.
     */
    protected function registerBladeExtensions()
    {
        $blade = $this->app['view']->getEngineResolver()->resolve('blade')->getCompiler();

        if (method_exists($blade, 'directive')) {
            $blade->directive('javascripts', function ($expression) {
                return "<?php echo basset_javascripts($expression); ?>";
            });

            $blade->directive('stylesheets', function ($expression) {
                return "<?php echo basset_stylesheets($expression); ?>";
            });
        } else {
            $blade->extend(function ($value, $compiler) {
                $matcher = $compiler->createMatcher('javascripts');

                return preg_replace($matcher, '$1<?php echo basset_javascripts$2; ?>', $value);
            });

            $blade->extend(function ($value, $compiler) {
                $matcher = $compiler->createMatcher('stylesheets');

                return preg_replace($matcher, '$1<?php echo basset_stylesheets$2; ?>', $value);
            });
        }
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return ['basset', 'Basset\Javascript'];
    }
}
